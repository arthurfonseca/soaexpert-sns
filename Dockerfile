FROM dockerfile/java

RUN mkdir /app
ADD target/standalone-tdc-2014.jar /app/server.jar
ADD src/main/resources/config.yml /app/config.yml

CMD ["java", "-jar", "/app/server.jar", "server", "/app/config.yml" ]

EXPOSE 8080