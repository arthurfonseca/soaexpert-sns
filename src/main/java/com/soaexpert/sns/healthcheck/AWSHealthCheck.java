package com.soaexpert.sns.healthcheck;

import javax.inject.Inject;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.hubspot.dropwizard.guice.InjectableHealthCheck;
import com.soaexpert.sns.SNSServiceConfiguration;

public class AWSHealthCheck extends InjectableHealthCheck {
    @Inject
    SNSServiceConfiguration cfg;

    @Inject
    AmazonS3 s3;

    @Inject
    AmazonDynamoDB dynamoDB;

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }

    @Override
    public String getName() {
        return "aws";
    }
}
